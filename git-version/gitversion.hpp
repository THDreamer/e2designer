#ifndef GITVERSION_HPP
#define GITVERSION_HPP

struct Git
{
    static const char* version;
};

#endif // GITVERSION_HPP
